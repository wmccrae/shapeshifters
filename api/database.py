events = [
    {
        "host_id": 1,
        "event_name": "Barton Creek Hike",
        "event_type": "hiking",
        "address_line1": "2207 Lou Neff Rd",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78746",
        "country": "US",
        "image_url": "https://communityimpact.com/uploads/images/2020/07/31/77423.jpg",
        "start_datetime": "2023-05-27 08:30:00",
        "end_datetime": "2023-05-27 11:30:00",
        "event_description": "Hike along Barton Creek"
    },
    {
        "host_id": 2,
        "event_name": "Swim Workout",
        "event_type": "swimming",
        "address_line1": "2201 William Barton Dr",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78746",
        "country": "US",
        "image_url": "https://www.austintexas.gov/sites/default/files/webimage-Barton-Springs-Pool.png",
        "start_datetime": "2023-05-28 08:30:00",
        "end_datetime": "2023-05-28 11:30:00",
        "event_description": "Swim laps with other swimming enthusiasts."
    },
    {
        "host_id": 4,
        "event_name": "Walk Lions Municipal Golf Course",
        "event_type": "walking",
        "address_line1": "2901 Enfield Rd",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78703",
        "country": "US",
        "image_url": "https://s3-media0.fl.yelpcdn.com/bphoto/l_9lrSBi68t59YwLsGUk3Q/l.jpg",
        "start_datetime": "2023-05-29 08:30:00",
        "end_datetime": "2023-05-29 11:30:00",
        "event_description": "We'll be walking around the perimeter of the golf course. Wear comfortable shoes."
    },
    {
        "host_id": 3,
        "event_name": "Barton Creek Greenbelt Hike",
        "event_type": "hiking",
        "address_line1": "3801 S Capital of Texas Hwy #250",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78704",
        "country": "US",
        "image_url": "https://s3.us-east-2.amazonaws.com/media.myelisting.com/listings/11/288584/3815-south-capital-of-texas-highway-austin-tx-78-8.jpg",
        "start_datetime": "2023-05-30 12:00:00",
        "end_datetime": "2023-05-30 3:30:00",
        "event_description": "Starting point is FlashParking."
    },
    {
        "host_id": 2,
        "event_name": "Cycle Veloway Park",
        "event_type": "biking",
        "address_line1": "4900 La Crosse Ave",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78739",
        "country": "US",
        "image_url": "https://naturerocksaustin.org/sites/default/files/styles/activity_more_img/public/greenspace/veloway.jpg",
        "start_datetime": "2023-05-31 15:00:00",
        "end_datetime": "2023-05-31 17:30:00",
        "event_description": "Cycling the path through Veloway Park."
    },
    {
        "host_id": 1,
        "event_name": "Austin Greenbelt Yoga",
        "event_type": "yoga",
        "address_line1": "3600 S Lamar Blvd",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78704",
        "country": "US",
        "image_url": "https://communityimpact.com/uploads/images/2020/07/31/77423.jpg",
        "start_datetime": "2023-05-15 09:00:00",
        "end_datetime": "2023-05-15 10:00:00",
        "event_description": "Join us for a refreshing yoga session on the greenbelt."
    },
    {
        "host_id": 2,
        "event_name": "Zilker Park Pilates",
        "event_type": "pilates",
        "address_line1": "2100 Barton Springs Rd",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78704",
        "country": "US",
        "image_url": "https://upload.wikimedia.org/wikipedia/commons/d/da/Austin-skyline.jpg",
        "start_datetime": "2023-05-17 10:00:00",
        "end_datetime": "2023-05-17 11:00:00",
        "event_description": "Get a great workout while enjoying the beauty of Zilker Park."
    },
    {
        "host_id": 3,
        "event_name": "Lady Bird Lake Hike",
        "event_type": "hiking",
        "address_line1": "1600 W Cesar Chavez St",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78703",
        "country": "US",
        "image_url": "https://www.gannett-cdn.com/presto/2021/09/29/NAAS/ed65f9d7-ac6e-4770-9526-f16483a62cc3-jwj_Trail_0335.jpg",
        "start_datetime": "2023-05-19 08:30:00",
        "end_datetime": "2023-05-19 11:00:00",
        "event_description": "Explore the beautiful trails around Lady Bird Lake."
    },
    {
        "host_id": 4,
        "event_name": "Town Lake Trail Run",
        "event_type": "running",
        "address_line1": "900 W Riverside Dr",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78704",
        "country": "US",
        "image_url": "https://s3-media0.fl.yelpcdn.com/bphoto/cjLAF_u8uG0RBBg-ZS3Tgw/348s.jpg",
        "start_datetime": "2023-05-21 06:30:00",
        "end_datetime": "2023-05-21 08:00:00",
        "event_description": "Join us for an invigorating run on the Town Lake trail."
    },
    {
        "host_id": 5,
        "event_name": "Lake Travis Kayak Tour",
        "event_type": "kayaking",
        "address_line1": "16107 FM2769",
        "address_line2": "",
        "city": "Leander",
        "state": "TX",
        "zip_code": "78641",
        "country": "US",
        "image_url": "https://cdn.laketravis.com/wp-content/uploads/2019/01/blog-laketravis.com-visting-lake-travis-header-01.jpg",
        "start_datetime": "2023-06-02 10:00:00",
        "end_datetime": "2023-06-02 12:00:00",
        "event_description": "Join us for a scenic kayak tour on Lake Travis."
    },
    {
        "host_id": 6,
        "event_name": "Barton Springs Pool Swim",
        "event_type": "swimming",
        "address_line1": "2201 Barton Springs Rd",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78746",
        "country": "US",
        "image_url": "https://www.austintexas.gov/sites/default/files/webimage-Barton-Springs-Pool.png",
        "start_datetime": "2023-06-03 11:00:00",
        "end_datetime": "2023-06-03 13:00:00",
        "event_description": "Take a refreshing swim in the natural spring-fed pool at Barton Springs."
    },
    {
        "host_id": 1,
        "event_name": "Lady Bird Lake Trail",
        "event_type": "running",
        "address_line1": "1000 Azie Morton Rd",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78704",
        "country": "US",
        "image_url": "https://www.gannett-cdn.com/presto/2021/09/29/NAAS/ed65f9d7-ac6e-4770-9526-f16483a62cc3-jwj_Trail_0335.jpg",
        "start_datetime": "2023-05-14 09:00:00",
        "end_datetime": "2023-05-31 10:30:00",
        "event_description": "Sunday - Runday We'll do a 8-10 mile run along the Lady Bird Lake Trail."
    },
    {
        "host_id": 1,
        "event_name": "Johnson Creek GreenBelt Run",
        "event_type": "Running",
        "event_description": "Humid weather makes this hike truly amazing. Bring the whole family and make memories!",
        "image_url": "https://cloudfront.traillink.com/photos/shoal-creek-trail_196616_nb.jpg",
        "start_datetime": "2023-06-01 12:30:00",
        "end_datetime": "2023-06-01 16:30:00",
        "address_line1": " 2001 Enfield Rd",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78703",
        "country": "USA"
    },
    {
        "host_id": 2,
        "event_name": "Swimming at Barton Springs Pool",
        "event_type": "Swimming",
        "event_description": "Cold water and high entry fees makes this event almost too good to pass up. Come not get your money's worth!",
        "image_url": "https://www.austintexas.gov/sites/default/files/webimage-Barton-Springs-Pool.png",
        "start_datetime": "2023-05-19 11:00:00",
        "end_datetime": "2023-05-19 17:30:00",
        "address_line1": "Barton Springs Rd",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78704",
        "country": "USA"
    },
    {
        "host_id": 3,
        "event_name": "Brushy Creek Nature Walk",
        "event_type": "Walking",
        "event_description": "A nice, chill walk through some really cool nature stuff.",
        "image_url": "https://www.pape-dawson.com/wp-content/uploads/E-Brushy-Creek-Trail-01.jpg",
        "start_datetime": "2023-07-01 10:30:00",
        "end_datetime": "2023-07-01 14:30:00",
        "address_line1": " Brushy Creek Regional Trail",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78717",
        "country": "USA",
    },
    {
        "host_id": 4,
        "event_name": "Hike and Bike",
        "event_type": "Hiking",
        "event_description": "Our semi-annual hike & bike event in beautiful Round Rock. Bring your own snacks.",
        "image_url": "https://naturerocksaustin.org/sites/default/files/styles/activity_more_img/public/greenspace/sarah-siller-photography-11-large.jpg",
        "start_datetime": "2023-05-13 13:00:00",
        "end_datetime": "2023-05-13 16:00:00",
        "address_line1": "3300 E Palm Valley Blvd",
        "address_line2": "",
        "city": "Round Rock",
        "state": "TX",
        "zip_code": "78665",
        "country": "USA"
    },
    {
        "host_id": 5,
        "event_name": "Pennybacker Bridge Hiking Extravaganza",
        "event_type": "Hiking",
        "event_description": "Come enjoy a mild hike, followed by beautiful views with the sunset!",
        "image_url": "https://texpainting.com/wp-content/uploads/2020/06/Penny-backer-bridge.jpg",
        "start_datetime": "2023-05-17 17:00:00",
        "end_datetime": "2023-05-17 19:30:00",
        "address_line1": "5300 N Capital of Texas Hw",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78730",
        "country": "USA"
    },
    {
        "host_id": 4,
        "event_name": "Wild Basin Wilderness Preserve",
        "event_type": "Hiking",
        "event_description": "Come for an easy 2.5 mile hike with waterfall views in Austin's first nature preserve. Bring a picnic if you'd like and stay after the hike.",
        "image_url": "https://naturerocksaustin.org/sites/default/files/styles/activity_more_img/public/greenspace/7687a0213d003bb913dc029670fa5b2a.jpg?itok=1y8WodQW",
        "start_datetime": "2023-05-20 16:30:00",
        "end_datetime": "2023-05-20 17:30:00",
        "address_line1": "805 N Capital of Texas Hwy",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78746",
        "country": "USA"
    },
    {
        "host_id": 1,
        "event_name": "Spicewood Valley Trail Hike",
        "event_type": "Hiking",
        "event_description": "Come get your sweat on and hike some hills, bring a friend and be miserable together!",
        "image_url": "https://img-aws.ehowcdn.com/700x/www.onlyinyourstate.com/wp-content/uploads/2019/05/image-2-6.jpg",
        "start_datetime": "2023-05-17 10:30:00",
        "end_datetime": "2023-05-17 17:00:00",
        "address_line1": "8043-8585 Scotland Well Dr",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78750",
        "country": "USA"
    },
    {
        "host_id": 3,
        "event_name": "Bird Watching Walk",
        "event_type": "walking",
        "address_line1": "2210 S FM 973",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78725",
        "country": "US",
        "image_url": "https://s3-media0.fl.yelpcdn.com/bphoto/FOrCJ7SGg3imEkWoCG5pBA/348s.jpg",
        "start_datetime": "2023-06-05 10:00:00",
        "end_datetime": "2023-06-05 12:30:00",
        "event_description": "Bring binoculars so you can look out for birds while we walk."
    },
    {
        "host_id": 1,
        "event_name": "Yoga at the Park",
        "event_type": "yoga",
        "address_line1": "109 Jacob Fontaine Ln",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78752",
        "country": "US",
        "image_url": "https://s3.us-east-2.amazonaws.com/media.myelisting.com/listings/3/160120/jacob-fontaine-ln-3.jpg?tr=w-300",
        "start_datetime": "2023-06-17 10:00:00",
        "end_datetime": "2023-06-17 11:00:00",
        "event_description": "Free yoga class at Jacob Fontaine Plaza Park."
    },
    {
        "host_id": 2,
        "event_name": "Kitten Yoga Benefitting Austin Pets Alive",
        "event_type": "yoga",
        "address_line1": "10401 Anderson Mill Rd",
        "address_line2": "#104",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78750",
        "country": "US",
        "image_url": "https://s3.us-east-2.amazonaws.com/media.myelisting.com/listings/3/160120/jacob-fontaine-ln-3.jpg?tr=w-300",
        "start_datetime": "2023-06-16 14:00:00",
        "end_datetime": "2023-06-16 15:00:00",
        "event_description": "$30 yoga class at Inner Diva Studios, all proceeds benefit Austin Pets Alive."
    },
    {
        "host_id": 3,
        "event_name": "Walk at Mueller Lake Park",
        "event_type": "hiking",
        "address_line1": "4550 Mueller Blvd",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78723",
        "country": "US",
        "image_url": "https://www.cnu.org/sites/default/files/mueller-austin-lake-park_0.jpg",
        "start_datetime": "2023-06-17 09:00:00",
        "end_datetime": "2023-06-17 10:00:00",
        "event_description": "Lets walk around Mueller Lake Park! Wear breathable clothing, and please remember to bring water and sunscreen."
    },
    {
        "host_id": 4,
        "event_name": "Morning Run",
        "event_type": "running",
        "address_line1": "2207 Lou Neff Rd",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78746",
        "country": "US",
        "image_url": "https://image-tc.galaxy.tf/wijpeg-w6or7mnm33j6fywe227tae6n/zilker-metropolitan-park-opt_standard.jpg",
        "start_datetime": "2023-06-24 08:00:00",
        "end_datetime": "2023-06-24 08:45:00",
        "event_description": "I'll be running 5 miles, feel free to join me!"
    },
    {
        "host_id": 5,
        "event_name": "Bike Trail Ride",
        "event_type": "biking",
        "address_line1": "900 W Riverside Dr",
        "address_line2": "",
        "city": "Austin",
        "state": "TX",
        "zip_code": "78704",
        "country": "US",
        "image_url": "https://lh5.googleusercontent.com/p/AF1QipPtJybrWLyyoC28lfGl_oGhQxq-QAK5O--ui5zq=w600-h321-p-k-no",
        "start_datetime": "2023-06-25 10:00:00",
        "end_datetime": "2023-06-25 11:30:00",
        "event_description": "We will be riding the Ann and Roy Butler Hike and Bike Trail."
    }
]

users = [
    {
        "first_name": "Carmen",
        "last_name": "Wheeler",
        "zip_code": "78750",
        "email": "carmen.wheeler@example.com",
        "hashed_password": "cameron1"
    },
    {
        "first_name": "Jorge",
        "last_name": "Reid",
        "zip_code": "78752",
        "email": "jorge.reid@example.com",
        "hashed_password": "houses"
    },
    {
        "first_name": "Rusty",
        "last_name": "Bannister",
        "zip_code": "78725",
        "email": "rusty.banniester@example.com",
        "hashed_password": "ernest"
    },
    {
        "first_name": "Darlene",
        "last_name": "Bailey",
		"zip_code": "78730",
        "email": "darlene.bailey@example.com",
        "hashed_password": "llllll"
    },
    {
        "first_name": "Yuri",
        "last_name": "Nator",
        "zip_code": "78717",
        "email": "yurinator@email.com",
        "hashed_password": "number1"
    },
    {
        "first_name": "Brandy",
        "last_name": "Soto",
        "zip_code": "78704",
        "email": "brandy.soto@email.com",
        "hashed_password": "number1"
    }
]
